Format: 3.0 (quilt)
Source: testprogramme
Binary: python3-testprogramme
Architecture: all
Version: 0.1-1
Maintainer: Théo Bouchevreau <theo.bouchevreau@bts-malraux.net>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-testprogramme deb python optional arch=all
Checksums-Sha1:
 88aca096789fb9f70bbe1e0dedc50d8063c3591d 1167 testprogramme_0.1.orig.tar.gz
 ba9e311c57ab238d6abef10ec8b7d92a74014415 708 testprogramme_0.1-1.debian.tar.xz
Checksums-Sha256:
 50c38bd61ad1ebbd03ada40d5262badcd2cf0867480eeb5b008100103f522d0b 1167 testprogramme_0.1.orig.tar.gz
 a12049b71004b9efc0452fbd70d8b06c8e9e1ebfef789f7b9e45e57683cca992 708 testprogramme_0.1-1.debian.tar.xz
Files:
 234887c8ec46b994564cd42438473dc6 1167 testprogramme_0.1.orig.tar.gz
 a8905e69a8aee7c785a3c19a7ed087da 708 testprogramme_0.1-1.debian.tar.xz
