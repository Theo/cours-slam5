from setuptools import setup

setup(
	name="testProgramme",
	version="0.1",
	author="Théo Bouchevreau",
	author_email="theo.bouchevreau@bts-malraux.net",
	description="petite description",
	long_description="grande, grande description",
	license="GPLv3",
	url="http://www.framagit.org",
	packages=['programme', 'programme.ressources'],
	entry_points={
		'console_scripts': [
			'test-executable = programme.main:run'
		]
	}
)
